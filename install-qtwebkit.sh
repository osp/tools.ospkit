
# Désinstaller le webkit installé éventuellement par le système
sudo apt remove libqt5webkit5 libqt5webkit5-dev 

# Installer les dépendances
sudo apt-get install build-essential perl python ruby flex gperf bison cmake \
ninja-build libfontconfig1-dev libicu-dev libsqlite3-dev zlib1g-dev libpng-dev \
libjpeg-dev libxslt1-dev libxml2-dev libhyphen-dev qtbase5-private-dev ccache

# Installer quelques libs Qt supplémentaires
sudo apt install qt5-default ruby-dev qtpositioning5-dev libqt5sensors5-dev libxcomposite-dev libglib2.0-dev libxrender-dev

# configurer ccache pour une compilation plus rapide en cas de soucis
sudo apt update && sudo apt install hyphen-fr

# Ajouter le chemin de ccache au fichier .bashrc
echo "export PATH=/usr/lib/ccache:$PATH" >| ~/.bashrc

# Créer un dossier webkit et télécharger toutes les sources nécessaires
mkdir -p ~/src/qtwebkit
cd ~/src/qtwebkit
wget https://github.com/annulen/webkit/releases/download/qtwebkit-5.212.0-alpha2/qtwebkit-5.212.0-alpha2.tar.xz
wget -O qt5-webkit-pdf_scaling.patch 'https://aur.archlinux.org/cgit/aur.git/tree/qt5-webkit-pdf_scaling.patch?h=qt5-webkit-print'
wget -O qt5-webkit-solid_background_PDF.patch 'https://aur.archlinux.org/cgit/aur.git/tree/qt5-webkit-solid_background_PDF.patch?h=qt5-webkit-print'
wget -O qt5-webkit-cmake3.10.patch https://gitlab.constantvzw.org/osp/tools.ospkit/raw/master/qtwebkit-patches/qt5-webkit-cmake3.10.patch?inline=false
wget -O qt5-webkit-gcc7.patch https://gitlab.constantvzw.org/osp/tools.ospkit/raw/master/qtwebkit-patches/qt5-webkit-gcc7.patch?inline=false
wget -O qt5-webkit-null-pointer-dereference.patch https://gitlab.constantvzw.org/osp/tools.ospkit/raw/master/qtwebkit-patches/qt5-webkit-null-pointer-dereference.patch?inline=false
wget -O qt5-webkit-trojita-1.patch https://gitlab.constantvzw.org/osp/tools.ospkit/raw/master/qtwebkit-patches/qt5-webkit-trojita-1.patch?inline=false
wget -O qt5-webkit-trojita-2.patch https://gitlab.constantvzw.org/osp/tools.ospkit/raw/master/qtwebkit-patches/qt5-webkit-trojita-2.patch?inline=false

mkdir build
tar xf qtwebkit-5.212.0-alpha2.tar.xz

# patcher les sources
cd qtwebkit-5.212.0-alpha2
patch -p1 -i ../qt5-webkit-pdf_scaling.patch
patch -p1 -i ../qt5-webkit-solid_background_PDF.patch
patch -p1 -i ../qt5-webkit-cmake3.10.patch
patch -p1 -i ../qt5-webkit-gcc7.patch
patch -p1 -i ../qt5-webkit-null-pointer-dereference.patch
patch -p1 -i ../qt5-webkit-trojita-1.patch
patch -p1 -i ../qt5-webkit-trojita-2.patch

# règle un bug de compilation sur certaines versions de debian/ubuntu
sed -e 's|-Wundef|-Wundef -Wno-expansion-to-defined|' -i Source/cmake/WebKitHelpers.cmake

# lancer la configuration
cd ../build
cmake ../qtwebkit-5.212.0-alpha2 -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DPORT=Qt -DENABLE_TOOLS=OFF -DENABLE_WEBKIT2=OFF -DENABLE_VIDEO=OFF -DENABLE_WEB_AUDIO=OFF

# Compiler
make
# or if you want to compile faster, use several processors, then you probably can't do something else at the same time
# make -j 4

# une fois la compilation finie sans erreur, installer webkit avec "sudo make install"
sudo make install

