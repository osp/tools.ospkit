# OSPKit


This browser is meant to be used with the project html2print available here: <http://osp.kitchen/tools/html2print/>.
The aim is to lay out printed documents within a web browser.
We built our own webkit browser in order to have a faster browser and good typography (weirdly, the bearings and kernings can be weird in certain webkit browsers).

## Manjaro


As the compilation of qt5webkit is long, we recently used this precompiled version https://sourceforge.net/projects/fabiololix-os-archive/ → Released /Packages/qt5-webkit-5.212.0alpha4-20-x86_64.pkg.tar.zst

To install it sudo pacman -U qt5-webkit-5.212.0alpha4-20-x86_64.pkg.tar.zst

Then install OSPKit

There is a package here, with everything needed

    sudo yay -S ospkit-git

If this package doesn't work

    git clone https://aur.archlinux.org/ospkit-git.git
    cd ospkit-git

Then change edit PKGBUILD

    sudo nano PKGBUILD

like that 

    -source=('git+http://gitlab.constantvzw.org/osp/tools.ospkit.git' 'ospkit.desktop' 'http://osp.kitchen/static/img/OSP_new-frog.svg')
    +source=('git+https://gitlab.constantvzw.org/osp/tools.ospkit.git' 'ospkit.desktop' 'http://osp.kitchen/static/img/OSP_new-frog.svg')

finally install it with 

    makepkg -sicr

## Ubuntu

You can find full detailed narrated info on osp blogpost http://blog.osp.kitchen/residency/ospkit-on-debian-11.html

### Download 

Download the zip file and decompress it (or git clone it), then go into the directory
    
    cd tools.ospkit

### Install QTwebkit 5.212

This is the part that makes OSPKit a timetravel machine! This version of Qt-Webkit and WebKit still supports the CSS Regions.

To install Qt-WebKit 5.212, it's easier to install the version shipped by the apt package manager, because building the OSP patch of Qt-WebKit 5.212 will take hours.

    $ sudo apt install libqt5webkit5
    $ sudo apt-get install qtbase5-dev
    $ sudo apt install libqt5webkit5-dev

### Install qmake

    $ sudo apt install qt5-qmake


### OSPKit Install
    
    cd src
    qmake
    make

## Launch the app

From the WebkitApp directory:

    ./OSPKit


## Available shortcuts

- Ctrl + P: Print to file
- Ctrl + Shift + P: Print dialog (allows output page resizing)
- Ctrl + R: Reload


    sudo ninja install


## Run OSPKit with this newly-built code


     cd ..
     
     ./OSPKit

Does the software launch itself with a black background? If not, send us feedback on miam@osp.kitchen (we will try to help if possible). If yes, a blank window is still not really exciting :) but we are preparing a boilerplate to start really to use it! 


## Césures françaises

Pour installer ce dictionnaire sur votre système:

    sudo apt update && sudo apt install hyphen-fr
    cd /usr/share/hyphen

Déplacer le dict système qu'on vient d'installer:

    sudo mv hyph_fr.dic hyph_fr.dic.orig

Remplacer le fichier par celui-ci https://gitlab.com/medor/hyph_fr/blob/master/hyph_fr_FR.dic

    sudo wget -O hyph_fr.dic https://gitlab.com/medor/hyph_fr/raw/master/hyph_fr_FR.dic

Redémarrer votre navigateur web, OSPkit ou logiciel qui se sert du dict de césures
